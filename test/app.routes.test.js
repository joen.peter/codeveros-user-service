const server = require('../app');
const request = require('supertest');

jest.mock('../lib/models/User');

const User = require('../lib/models/User');

// close the server after each test
afterEach(() => {
  server.close();
});

describe('Routes', () => {
  test('GET /api/user should respond as expected', async () => {
    const expected = [ { _id: '1', firstName: 'Test', lastName: 'User' }, { _id: '2', firstName: 'Test 2', lastName: 'User 2' } ];
    User.find.mockReturnValueOnce(expected);
    const response = await request(server).get('/api/user');
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(expected);
  });

  test('GET /api/user/:id with valid id should respond as expected', async () => {
    const _id = '1';
    const expected = { _id, firstName: 'Test', lastName: 'User' };
    User.findById.mockReturnValueOnce(expected);
    const response = await request(server).get(`/api/user/${_id}`);
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(expected);
  });

  test('GET /api/user/:id with invalid id should return a 404', async () => {
    User.findById.mockReturnValueOnce(null);
    const response = await request(server).get('/api/user/500');
    expect(response.status).toEqual(404);
  });

  test('POST /api/user should respond as expected', async () => {
    const expected = { _id: '1', firstName: 'Test', lastName: 'User' };
    User.create.mockReturnValueOnce(expected);
    const response = await request(server).post('/api/user').send({ firstName: 'Test', lastName: 'User' });
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(expected);
  });

  test('PUT /api/user/:id with valid id should respond as expected', async () => {
    const _id = '1';
    const original = { _id, firstName: 'Old', lastName: 'User' };
    const updated = { _id, firstName: 'New', lastName: 'User' };
    User.findById.mockReturnValueOnce(original);
    User.findByIdAndUpdate.mockReturnValueOnce(updated);
    const response = await request(server).put(`/api/user/${_id}`).send({ firstName: 'New', lastName: 'User' });
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(updated);
  });

  test('DELETE /api/user/:id with valid id should respond as expected', async () => {
    const _id = '1';
    const expected = { _id, firstName: 'Old', lastName: 'User' };
    User.findById.mockReturnValueOnce(expected);
    User.findByIdAndRemove.mockReturnValueOnce(expected);
    const response = await request(server).delete(`/api/user/${_id}`);
    expect(response.status).toEqual(200);
    expect(response.type).toEqual('application/json');
    expect(response.body).toEqual(expected);
  });
});
